using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Chapters.Chapter03
{
	public class Arc : Shape
	{
		protected override Geometry DefiningGeometry
		{
			get { return GetArcGeometry2(); }
		}

        static Arc()
        {
            StretchProperty.OverrideMetadata(typeof(Arc), new FrameworkPropertyMetadata(Stretch.Fill));
        }

		public static readonly DependencyProperty StartAngleProperty = DependencyProperty.Register(
			"StartAngle", typeof (double), typeof (Arc),
			new FrameworkPropertyMetadata(0D, FrameworkPropertyMetadataOptions.AffectsRender));

		public static readonly DependencyProperty EndAngleProperty = DependencyProperty.Register(
			"EndAngle", typeof (double), typeof (Arc),
			new FrameworkPropertyMetadata(0D, FrameworkPropertyMetadataOptions.AffectsRender));

		public double StartAngle
		{
			get { return (double) GetValue(StartAngleProperty); }
			set { SetValue(StartAngleProperty, value); }
		}

		public double EndAngle
		{
			get { return (double) GetValue(EndAngleProperty); }
			set { SetValue(EndAngleProperty, value); }
		}

		private Geometry GetArcGeometry()
		{
            Debug.WriteLine(string.Format("Rendersize:{0}, {1}, StrokeThickness:{2}", RenderSize.Width, RenderSize.Height, StrokeThickness));
			Point startPoint = PointAtAngle(RenderSize, Math.Min(StartAngle, EndAngle));
			Point endPoint = PointAtAngle(RenderSize, Math.Max(StartAngle, EndAngle));

			Size arcSize = new Size(Math.Max(0, (RenderSize.Width - StrokeThickness)/2),
			                        Math.Max(0, (RenderSize.Height - StrokeThickness)/2));
			bool isLargeArc = Math.Abs(EndAngle - StartAngle) > 180;

			StreamGeometry geom = new StreamGeometry();
			using (StreamGeometryContext context = geom.Open())
			{
				context.BeginFigure(startPoint, false, false);
				context.ArcTo(endPoint, arcSize, 0, isLargeArc, SweepDirection.Counterclockwise, true, false);
			}

			geom.Transform = new TranslateTransform(StrokeThickness/2, StrokeThickness/2);
			return geom;
		}

		private Geometry GetArcGeometry2()
		{
			Point startPoint = PointAtAngle(RenderSize, Math.Min(StartAngle, EndAngle));
			Point endPoint = PointAtAngle(RenderSize, Math.Max(StartAngle, EndAngle));
			Point stopPoint = PointAtAngle(RenderSize, Math.Max(EndAngle, 359.99));

			Size arcSize = new Size(Math.Max(0, (RenderSize.Width - StrokeThickness)/2),
			                        Math.Max(0, (RenderSize.Height - StrokeThickness)/2));
			bool isLargeArc = Math.Abs(EndAngle - StartAngle) > 180;

			StreamGeometry geom = new StreamGeometry();
			using (StreamGeometryContext context = geom.Open())
			{
				context.BeginFigure(startPoint, false, false);
				context.ArcTo(endPoint, arcSize, 0, isLargeArc, SweepDirection.Counterclockwise, true, false);
				context.ArcTo(stopPoint, arcSize, 0, !isLargeArc, SweepDirection.Counterclockwise, false, false);
			}

			geom.Transform = new TranslateTransform(StrokeThickness/2, StrokeThickness/2);
			return geom;
		}



		private Point PointAtAngle(Size pRenderSize, double angle)
		{
			double radAngle = angle*(Math.PI/180);
			double xRadius = (pRenderSize.Width - StrokeThickness)/2;
			double yRadius = (pRenderSize.Height - StrokeThickness)/2;

			double x = xRadius + xRadius*Math.Cos(radAngle);
			double y = yRadius - yRadius*Math.Sin(radAngle);

			return new Point(x, y);
		}

        protected override Size MeasureOverride(Size availableSize)
        {
            Size desiredSize = base.MeasureOverride(availableSize);
            //RenderSize = availableSize;
            return desiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            //Debug.WriteLine(string.Format("ArrangeOverride:{0}, {1}", finalSize.Width, finalSize.Height));
            base.ArrangeOverride(finalSize);
            return finalSize;
        }
	}
}