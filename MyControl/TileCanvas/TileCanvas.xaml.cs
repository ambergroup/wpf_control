﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TileControl
{
    /// <summary>
    /// Interaction logic for TileCanvas.xaml
    /// </summary>
    public partial class TileCanvas : UserControl
    {
        static TileCanvas()
        {
        }

        public TileCanvas()
        {
            InitializeComponent();
            this.RenderTransform = new TransformGroup()
            {
                Children = new TransformCollection()
                {
                    translateTransform, 
                    matrixTransform
                }
            };
            this.TileToolTip.RenderTransform = new TransformGroup()
            {
                Children = new TransformCollection()
                {
                    tp_matrixTransform
                }
            };
        }

        private MatrixTransform matrixTransform = new MatrixTransform();
        private TranslateTransform translateTransform = new TranslateTransform();
        private MatrixTransform tp_matrixTransform = new MatrixTransform();
        private bool IsPress = false;
        private Point OldPoint;

        class ScalePosition
        {
            public double X;
            public double Y;
            public double Scale;
        }

        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var element = sender as UIElement;
            var position = e.GetPosition(element);
            var scale = e.Delta >= 0 ? 1.1 : (1.0 / 1.1); 
            ZoomTransform(matrixTransform, new ScalePosition(){X=position.X + translateTransform.X, Y= position.Y + translateTransform.Y, Scale = scale});
            ZoomTransform(tp_matrixTransform, new ScalePosition(){X=position.X, Y = position.Y, Scale = 1/scale});
        }

        //public static readonly DependencyProperty ScaleValueProperty = DependencyProperty.Register(
        //    "ScaleValue", typeof (double), typeof (TileCanvas), new PropertyMetadata(default(double), OnScaleValueChange));

        //private static void OnScaleValueChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        //{
        //    var element = dependencyObject as TileCanvas;
        //    var position = new Point(element.Width/2, element.Height/2);
        //    var scale = (double)dependencyPropertyChangedEventArgs.NewValue; 
        //    ZoomTransform(element.matrixTransform, new ScalePosition(){X=position.X, Y= position.Y, Scale = scale});
        //}

        //public double ScaleValue
        //{
        //    get { return (double) GetValue(ScaleValueProperty); }
        //    set { SetValue(ScaleValueProperty, value); }
        //}

        private static void ZoomTransform(MatrixTransform matrixTransform, ScalePosition sp)
        {
            var matrix = matrixTransform.Matrix;
            matrix.ScaleAtPrepend(sp.Scale, sp.Scale, sp.X, sp.Y);
            matrixTransform.Matrix = matrix;
        }

        public static readonly DependencyProperty RowCountProperty = DependencyProperty.Register(
            "RowCount", typeof (int), typeof (TileCanvas), new PropertyMetadata(default(int), OnRowCountPropertyChange));

        private static void OnRowCountPropertyChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var control = dependencyObject as TileCanvas;
            if (control == null)
            {
                return;
            }
            ArrangeTiles(control, control.RowCount, control.ColumnCount, control.TileWidth, control.TileHeight);
        }

        public int RowCount
        {
            get { return (int) GetValue(RowCountProperty); }
            set { SetValue(RowCountProperty, value); }
        }

        public static readonly DependencyProperty ColumnCountProperty = DependencyProperty.Register(
            "ColumnCount", typeof (int), typeof (TileCanvas), new PropertyMetadata(default(int), OnColumnCountPropertyChange));

        private static void OnColumnCountPropertyChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var control = dependencyObject as TileCanvas;
            if (control == null)
            {
                return;
            }
            ArrangeTiles(control, control.RowCount, control.ColumnCount, control.TileWidth, control.TileHeight);
        }

        public int ColumnCount
        {
            get { return (int) GetValue(ColumnCountProperty); }
            set { SetValue(ColumnCountProperty, value); }
        }

        public static readonly DependencyProperty TileCanvasWidthProperty = DependencyProperty.Register(
            "TileCanvasWidth", typeof (int), typeof (TileCanvas), new PropertyMetadata(default(int)));

        public int TileCanvasWidth
        {
            get { return (int) GetValue(TileCanvasWidthProperty); }
            set { SetValue(TileCanvasWidthProperty, value); }
        }

        public static readonly DependencyProperty TileCanvasHeightProperty = DependencyProperty.Register(
            "TileCanvasHeight", typeof (int), typeof (TileCanvas), new PropertyMetadata(default(int)));

        public int TileCanvasHeight
        {
            get { return (int) GetValue(TileCanvasHeightProperty); }
            set { SetValue(TileCanvasHeightProperty, value); }
        }

        public static readonly DependencyProperty TileWidthProperty = DependencyProperty.Register(
            "TileWidth", typeof (int), typeof (TileCanvas), new PropertyMetadata(default(int), OnTileWidthChange));

        private static void OnTileWidthChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var control = dependencyObject as TileCanvas;
            if (control == null)
            {
                return;
            }
            ArrangeTiles(control, control.RowCount, control.ColumnCount, control.TileWidth, control.TileHeight);
            control.TileLineThickness = control.TileWidth*0.01;
        }

        public int TileWidth
        {
            get { return (int) GetValue(TileWidthProperty); }
            set { SetValue(TileWidthProperty, value); }
        }

        public static readonly DependencyProperty TileHeightProperty = DependencyProperty.Register(
            "TileHeight", typeof (int), typeof (TileCanvas), new PropertyMetadata(default(int), OnTileHeightChange));

        private static void OnTileHeightChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var control = dependencyObject as TileCanvas;
            if (control == null)
            {
                return;
            }
            ArrangeTiles(control, control.RowCount, control.ColumnCount, control.TileWidth, control.TileHeight);
        }

        public int TileHeight
        {
            get { return (int) GetValue(TileHeightProperty); }
            set { SetValue(TileHeightProperty, value); }
        }

        public static readonly DependencyProperty SelectedPositionProperty = DependencyProperty.Register(
            "SelectedPosition", typeof (Thickness), typeof (TileCanvas), new PropertyMetadata(default(Thickness)));

        public Thickness SelectedPosition
        {
            get { return (Thickness) GetValue(SelectedPositionProperty); }
            set { SetValue(SelectedPositionProperty, value); }
        }

        public static readonly DependencyProperty SelectedTileIdProperty = DependencyProperty.Register(
            "SelectedTileId", typeof (int), typeof (TileCanvas), new PropertyMetadata(default(int), OnSelectedTileIdChange));

        private static void OnSelectedTileIdChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var control = dependencyObject as TileCanvas;
            if (control != null)
            {
                var tileId = (int) dependencyPropertyChangedEventArgs.NewValue;
                if (tileId <= 0)
                {
                    control.SelectedTileCtrl.Visibility = Visibility.Hidden;
                }
                else
                {
                    if (control.TileIdRectDic != null && control.TileIdRectDic.ContainsKey(tileId))
                    {
                        control.SelectedTileCtrl.Visibility = Visibility.Visible;
                        var left = control.TileIdRectDic[tileId].Left;
                        var top = control.TileIdRectDic[tileId].Top;
                        control.SelectedPosition = new Thickness(left, top, 0, 0);
                    }
                }
            }
        }

        public int SelectedTileId
        {
            get { return (int) GetValue(SelectedTileIdProperty); }
            set { SetValue(SelectedTileIdProperty, value); }
        }

        public static readonly DependencyProperty TileRectIdDicProperty = DependencyProperty.Register(
            "TileRectIdDic", typeof (Dictionary<Rect,int>), typeof (TileCanvas), new PropertyMetadata(default(Dictionary<Rect,int>), OnTileRectIdDicChange));

        private static void OnTileRectIdDicChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var control = dependencyObject as TileCanvas;
            control.TileIdRectDic = control.TileRectIdDic.ToDictionary(x => x.Value, x => x.Key);
        }

        public Dictionary<Rect,int> TileRectIdDic
        {
            get { return (Dictionary<Rect,int>) GetValue(TileRectIdDicProperty); }
            set { SetValue(TileRectIdDicProperty, value); }
        }

        public Dictionary<int, Rect> TileIdRectDic { get; set; } 

        private static void ArrangeTiles(TileCanvas pThis, int pRowCount, int pColumnCount, int pTileWidth, int pTileHeight)
        {
            //pThis.TileCanvasHeight = pRowCount*pTileHeight;
            //pThis.TileCanvasWidth = pColumnCount*pTileWidth;
            pThis.TileRect = new Rect(0,0, pTileWidth, pTileHeight);
        }

        public static readonly DependencyProperty TileLineThicknessProperty = DependencyProperty.Register(
        "TileLineThickness", typeof(double), typeof(TileCanvas), new PropertyMetadata(default(double)));

        public double TileLineThickness
        {
            get { return (double)GetValue(TileLineThicknessProperty); }
            set { SetValue(TileLineThicknessProperty, value); }
        }

        public static readonly DependencyProperty TileRectProperty = DependencyProperty.Register(
            "TileRect", typeof (Rect), typeof (TileCanvas), new PropertyMetadata(new Rect(0,0,10,10)));

        public Rect TileRect
        {
            get { return (Rect) GetValue(TileRectProperty); }
            set { SetValue(TileRectProperty, value); }
        }

        public static readonly DependencyProperty TileInfoProperty = DependencyProperty.Register(
            "TileInfo", typeof (string), typeof (TileCanvas), new PropertyMetadata(default(string)));

        public string TileInfo
        {
            get { return (string) GetValue(TileInfoProperty); }
            set { SetValue(TileInfoProperty, value); }
        }

        public int DumbHalfTileWidth { get; set; }

        public int DumbHalfTileHeight { get; set; }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right)
            {
                var p = Mouse.GetPosition((IInputElement) sender);
                SelectedTileCtrl.Visibility = Visibility.Visible;
                if (TileRectIdDic == null)
                {
                    return;
                }
                var tileItem = TileRectIdDic.Where(x => x.Key.Contains(p));
                if (tileItem.Count() == 1)
                {
                    SelectedTileId = tileItem.First().Value;
                }
            }
            else if (e.ChangedButton == MouseButton.Left)
            {
                IsPress = false;
            }
            else { }
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            var p = Mouse.GetPosition((IInputElement) sender);
            if (TileRectIdDic != null)
            {
                var tileItem = TileRectIdDic.Where(x => x.Key.Contains(p));
                if (tileItem.Count() == 1)
                {
                    TileToolTip.IsOpen = true;
                    TileInfo = "     Tile " + tileItem.First().Value.ToString();
                    TileToolTip.HorizontalOffset = p.X;
                    TileToolTip.VerticalOffset = p.Y;
                }
                else
                {
                    TileToolTip.IsOpen = false;
                }
            }

            if (IsPress)
            {
                var CurrentPoint = e.GetPosition(this);
                translateTransform.X += (CurrentPoint.X - OldPoint.X );
                translateTransform.Y += (CurrentPoint.Y - OldPoint.Y );
            }
        }

        //private void AddToolTip(Grid sender, MouseEventArgs e)
        //{
        //    var tooltip = new ToolTip();
        //}

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            TileToolTip.IsOpen = false;
            //TileToolTip.Visibility = Visibility.Hidden;
            IsPress = false;
            InvalidateVisual();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (1 == e.ClickCount)
                {
                    IsPress = true;
                    OldPoint = e.GetPosition(this);
                }
                else if (2 == e.ClickCount)
                {
                    translateTransform = new TranslateTransform();
                    matrixTransform = new MatrixTransform();
                    this.RenderTransform = new TransformGroup()
                    {
                        Children = new TransformCollection()
                        {
                            translateTransform,
                            matrixTransform
                        }
                    };
                }
                else{}
            }
        }
    }
}
