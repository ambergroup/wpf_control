﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfPedestalCanvas
{
    public enum RegionShape { None, Point, Line, Rectangle, Ellipse, Polygon, Gate }

    public class PlateCanvas: Canvas
    {
        private double _LineWidth = 1.0;
        private RegionShape ShapeType;
        
        private readonly Dictionary<string, Rect> _roomRectDic = new Dictionary<string, Rect>();
        private readonly Dictionary<string, Rect> _vHeaderDic = new Dictionary<string, Rect>();
        private readonly Dictionary<string, Rect> _hHeaderDic = new Dictionary<string, Rect>();

        private int RowCount = 9;
        private int ColumnCount = 12;
        private double ActualScale = 100;
        private double XInterval = 10;
        private double YInterval = 10;
        private double LineWidth = 0.5;
        private double WellSizeXIntervalRatio = 0.8;
        private double WellSizeYIntervalRatio = 0.8;
        private ScaleTransform scaleTransform = new ScaleTransform();
        private TranslateTransform translateTransform = new TranslateTransform();
        private MatrixTransform matrixTransform = new MatrixTransform();
        private TransformGroup canvasTansform;

        public TransformGroup CanvasTransform
        {
            get { return canvasTansform; }
            set { canvasTansform = value; }
        }

        static PlateCanvas()
        {
        }

        public PlateCanvas()
        {
            ShapeType = RegionShape.Rectangle;
            InitHHeaderDictionary();
            InitVHeaderDictionary();
            InitRoomList();
            Width = (ColumnCount + 1)*XInterval;
            Height = (RowCount + 1)*YInterval;
            //this.SetBinding(RenderTransformProperty, "CanvasTransform");
            canvasTansform = new TransformGroup()
            {
                Children = new TransformCollection()
                {
                    translateTransform, 
                    scaleTransform,
                    matrixTransform
                }
            };
            this.RenderTransform = canvasTansform;
        }

        private bool IsPress = false;
//        private bool IsMove = true;
        private Point OldPoint;
        private Point ZoomCenter;

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            ZoomMethod1(e);
        }

        private void ZoomMethod4(MouseWheelEventArgs e)
        {
            Point mousePos = e.GetPosition(this);

            //oldCenter : untransformed and transformed
            Point untransformedOldCenter = new Point(this.scaleTransform.CenterX, this.scaleTransform.CenterY);
            Point transformedOldCenter = this.scaleTransform.Transform(untransformedOldCenter);

            //newCenter : untransformed and transformed
            Point untransformedNewCenter = this.RenderTransform.Inverse.Transform(mousePos);
            Point transformedNewCenter = mousePos;

            double adjustX = transformedNewCenter.X - transformedOldCenter.X - untransformedNewCenter.X + untransformedOldCenter.X;
            double adjustY = transformedNewCenter.Y - transformedOldCenter.Y - untransformedNewCenter.Y + untransformedOldCenter.Y;

            //update transforms
            this.translateTransform.X = adjustX;
            this.scaleTransform.CenterX = untransformedNewCenter.X;
            this.scaleTransform.ScaleX += e.Delta/100;
            this.translateTransform.Y = adjustY;
            this.scaleTransform.CenterY = untransformedNewCenter.Y;
            this.scaleTransform.ScaleY += e.Delta/100;
        }

        private void ZoomMethod3(MouseWheelEventArgs e)
        {
            var element = this as UIElement;
            var position = e.GetPosition(element);
            var matrix = matrixTransform.Matrix;
            var scale = e.Delta >= 0 ? 1.1 : (1.0 / 1.1); 

            matrix.ScaleAtPrepend(scale, scale, position.X + translateTransform.X, position.Y + translateTransform.Y);
            matrixTransform.Matrix = matrix;
        }

        private void ZoomMethod2(MouseWheelEventArgs e)
        {
            e.Handled = true;
            Focus();
            var tWidth = this.ActualWidth;
            var tHeight = this.ActualHeight;
            var parent = LogicalTreeHelper.GetParent(this);
            //var screePos = e.GetPosition(this);
            ZoomCenter = e.GetPosition((IInputElement)parent);//canvasTansform.Inverse.Transform(parent);
            var pt = canvasTansform.Inverse.Transform(ZoomCenter);
            //var ScreePoint = this.PointToScreen(ZoomCenter);
            Debug.WriteLine("Before Curr:" + ZoomCenter.ToString());
            //Debug.WriteLine("SX:" + scaleTransform.ScaleX);
            //Debug.WriteLine("SY:" + scaleTransform.ScaleY);
            //Debug.WriteLine("TX:" + translateTransform.X);
            //Debug.WriteLine("TY:" + translateTransform.Y);
            //Debug.WriteLine("CX:" + scaleTransform.CenterX);
            //Debug.WriteLine("CY:" + scaleTransform.CenterY);

            translateTransform.X = (ZoomCenter.X-pt.X) * scaleTransform.ScaleX;
            translateTransform.Y = (ZoomCenter.Y-pt.Y) * scaleTransform.ScaleY;
            //translateTransform.Y = (-1) * (OriginPoint.Y * scaleTransform.ScaleY - ZoomCenter.Y);
            scaleTransform.CenterX = ZoomCenter.X;
            scaleTransform.CenterY = ZoomCenter.Y;
            if (e.Delta > 0)
            {
                scaleTransform.ScaleX *= 1.1;
                scaleTransform.ScaleY *= 1.1;
            }
            else
            {
                scaleTransform.ScaleX /= 1.1;
                scaleTransform.ScaleY /= 1.1;
            }
            //Thread.Sleep(100);
            //ZoomCenter = this.PointFromScreen(ScreePoint);
            //ZoomCenter = e.GetPosition((IInputElement) parent);
            //Debug.WriteLine(" After Curr:" + ZoomCenter.ToString());
            //Debug.WriteLine("===============================");
        }

        private void ZoomMethod1(MouseWheelEventArgs e)
        {
            //e.Handled = true;
            //Focus();
            var tWidth = this.ActualWidth;
            var tHeight = this.ActualHeight;
            ////var parent = VisualTreeHelper.GetParent(this);
            //Point CurrentPoint = new Point();
            //if (e.LeftButton == MouseButtonState.Pressed)
            //{
            //    CurrentPoint = e.GetPosition(this);
            //}
            var element = this as UIElement;
            var CurrentPoint = e.GetPosition(element);
            //var ScreePoint = this.PointToScreen(ZoomCenter);
            Debug.WriteLine("Before Curr:" + CurrentPoint.ToString());
            //Debug.WriteLine("SX:" + scaleTransform.ScaleX);
            //Debug.WriteLine("SY:" + scaleTransform.ScaleY);
            //Debug.WriteLine("TX:" + translateTransform.X);
            //Debug.WriteLine("TY:" + translateTransform.Y);
            //Debug.WriteLine("CX:" + scaleTransform.CenterX);
            //Debug.WriteLine("CY:" + scaleTransform.CenterY);

            var oldscale = scaleTransform.ScaleX;
            scaleTransform = new ScaleTransform();
            double scale = 1;
            if (e.Delta > 0)
            {
                scale = scale*1.1;
            }
            else
            {
                scale = scale/1.1;
            }
            scaleTransform.ScaleX = oldscale * scale;
            scaleTransform.ScaleY = oldscale * scale;
            scaleTransform.CenterX = CurrentPoint.X + translateTransform.X;
            scaleTransform.CenterY = CurrentPoint.Y + translateTransform.Y;
            //scaleTransform = st;
            translateTransform = new TranslateTransform();
            this.RenderTransform = new TransformGroup()
            {
                Children = new TransformCollection()
                {
                    scaleTransform,
                    translateTransform
                }
            };

            //Thread.Sleep(100);
            //ZoomCenter = this.PointFromScreen(ScreePoint);
            CurrentPoint = e.GetPosition(this);
            Debug.WriteLine(" After Curr:" + CurrentPoint.ToString());
            Debug.WriteLine("===============================");
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            IsPress = true;
            OldPoint = e.GetPosition(this);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsPress)
            {
                var CurrentPoint = e.GetPosition(this);
                translateTransform.X += (CurrentPoint.X - OldPoint.X );
                translateTransform.Y += (CurrentPoint.Y - OldPoint.Y );
                Debug.WriteLine("MCurr:" + CurrentPoint.ToString());
                Debug.WriteLine("MTX:" + translateTransform.X);
                Debug.WriteLine("MTY:" + translateTransform.Y);
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            IsPress = false;
            if (!CheckHHeadersSelect(e))
            {
                if (!CheckVHeadersSelect(e))
                {
                    CheckRegionsSelect(e);
                }
            }
        }

        private bool CheckVHeadersSelect(MouseButtonEventArgs e)
        {
            return true;
        }

        private bool CheckHHeadersSelect(MouseButtonEventArgs e)
        {
            return true;
        }

        private void CheckRegionsSelect(MouseButtonEventArgs e)
        {}
        
        protected override void OnLostMouseCapture(MouseEventArgs e)
        {
            IsPress = false;
        }

        private void InitVHeaderDictionary()
        {
            for (int i = 0; i < RowCount; i ++)
            {
                var rect = new Rect(new Point()
                {
                    X = 0,
                    Y = GetTitleSpace(YInterval) + i*YInterval
                }, new Size()
                {
                    Width = GetTitleSpace(XInterval),
                    Height = YInterval
                });
                _vHeaderDic.Add((i+1).ToString(), rect);
            }
        }

        private void InitHHeaderDictionary()
        {
            for (int i = 0; i < ColumnCount; i ++)
            {
                var rect = new Rect(new Point()
                {
                    X = GetTitleSpace(XInterval) + i*XInterval,
                    Y = 0
                }, new Size()
                {
                    Width = XInterval,
                    Height = GetTitleSpace(YInterval)
                });
                _hHeaderDic.Add(((char)(i+65)).ToString(), rect);
            }
        }

        private void InitRoomList()
        {
            for (var row = 0; row < RowCount; row++)
            {
                for (var col = 0; col < ColumnCount; col++)
                {
                    var rect = new Rect(new Point
                    {
                        X = col*XInterval + GetTitleSpace(XInterval), 
                        Y = row*YInterval + GetTitleSpace(YInterval)
                    }, new Size()
                    {
                        Width = XInterval,
                        Height = YInterval
                    });

                    //var scale = ActualScale/100;
                    //rect.X *= scale;
                    //rect.Y *= scale;
                    //rect.Width *= scale;
                    //rect.Height *= scale;

                    _roomRectDic.Add(GetPlateId(row, col), rect);
                }
            }

        }

        private string GetPlateId(int row, int col)
        {

            var strRow = GetRow(row);
            var strCol = (col + 1).ToString();
            return strRow + strCol;
        }

        private string GetRow(int row)
        {
            int[] a = new int[2];
            a[1] = row / 26;
            a[0] = row % 26;
            string result = string.Empty;
            if (a[1] > 0)
            {
                result = string.Format("{0}{1}", (char)(65 + a[1]), (char)(65 + a[0]));
            }
            else
            { 
                result = string.Format("{0}", (char)(65 + a[0]));
            }
            return result;
        }

        private double GetTitleSpace(double pInterval)
        {
            return pInterval;
        }

        private void DrawRooms(DrawingContext dc)
        {
            var roomBackground = Brushes.DarkGray;
           _roomRectDic.ToList().ForEach(x =>
           {
                DrawFunction.DrawRectangle(dc, roomBackground, new Pen(Brushes.WhiteSmoke, LineWidth), x.Value);
           }); 
        }

        private void DrawWells(DrawingContext dc)
        {
            var wellBackground = Brushes.WhiteSmoke;
            var wellPenBrush = Brushes.Pink;
            _roomRectDic.ToList().ForEach(x =>
            {
                var center = new Point()
                {
                    X = (x.Value.Left + x.Value.Right)*0.5,
                    Y = (x.Value.Top + x.Value.Bottom)*0.5
                };
                var radiusX = x.Value.Width*0.5*WellSizeXIntervalRatio;
                var radiusY = x.Value.Height*0.5*WellSizeYIntervalRatio;
                if (RegionShape.Rectangle == ShapeType)
                {
                    dc.DrawRectangle(wellBackground,
                        new Pen(Brushes.LightPink, LineWidth),
                        new Rect(
                            center.X - radiusX * 0.5,
                            center.Y - radiusY * 0.5,
                            radiusX,
                            radiusY));
                }
                else if (RegionShape.Ellipse == ShapeType)
                {
                    dc.DrawEllipse(wellBackground,
                        new Pen(wellPenBrush, LineWidth),
                        center,
                        radiusX,
                        radiusY);
                }
                else
                {
                    Debug.Assert(false, "Illegal Shape");
                }
            });
        }

        private void DrawTitles(DrawingContext dc)
        {
            var ftsize = 16.0;
            var ftsizewidthscale = 0.56;
            _vHeaderDic.ToList().ForEach(x =>
            {
                var center = new Point()
                {
                    X = (x.Value.Left + x.Value.Right)*0.5,
                    Y = (x.Value.Top + x.Value.Bottom)*0.5
                };
                ftsize = x.Value.Height*ftsizewidthscale;
                var ft = new FormattedText(x.Key, CultureInfo.InvariantCulture, FlowDirection.LeftToRight,
                    new Typeface("Verdana"), ftsize, Brushes.LightGray);
                dc.DrawText(ft,
                    new Point(center.X-ft.Width/2, center.Y - ft.Height/2));
            });
            _hHeaderDic.ToList().ForEach(x =>
            {
                var center = new Point()
                {
                    X = (x.Value.Left + x.Value.Right)*0.5,
                    Y = (x.Value.Top + x.Value.Bottom)*0.5
                };
                ftsize = x.Value.Height*ftsizewidthscale;
                var ft = new FormattedText(x.Key, CultureInfo.InvariantCulture, FlowDirection.LeftToRight,
                    new Typeface("Verdana"), ftsize, Brushes.LightGray);
                dc.DrawText(ft,
                    new Point(center.X-ft.Width/2, center.Y - ft.Height/2));
            });
        }

        protected override void OnRender(DrawingContext dc)
        {
            Background = Brushes.DimGray;

            DrawRooms(dc);
            DrawWells(dc);
            DrawTitles(dc);
        }
    }

    public class DrawFunction
    {
        public static void DrawLine(DrawingContext dc, Pen pen, Point pt0, Point pt1)
        {
            var halfPenWidth = pen.Thickness / 2;
            var g = new GuidelineSet();
            g.GuidelinesX.Add(pt0.X + halfPenWidth);
            g.GuidelinesX.Add(pt1.X + halfPenWidth);
            g.GuidelinesY.Add(pt0.Y + halfPenWidth);
            g.GuidelinesY.Add(pt1.Y + halfPenWidth);
            dc.PushGuidelineSet(g);
            dc.DrawLine(pen, pt0, pt1);
            dc.Pop();
        }

        public static void DrawRectangle(DrawingContext dc, Brush brush, Pen pen, Rect rect)
        {
            if (pen != null)
            {
                var halfPenWidth = pen.Thickness / 2;
                var g = new GuidelineSet();
                g.GuidelinesX.Add(rect.Left + halfPenWidth);
                g.GuidelinesX.Add(rect.Right + halfPenWidth);
                g.GuidelinesY.Add(rect.Top + halfPenWidth);
                g.GuidelinesY.Add(rect.Bottom + halfPenWidth);

                dc.PushGuidelineSet(g);
                dc.DrawRectangle(brush, pen, rect);
                dc.Pop();
            }
            else
            {
                dc.DrawRectangle(brush, null, rect);
            }

        }

        public static void DrawGeometry(DrawingContext dc, Brush brush, Pen pen, PathGeometry pathGeometry, Point[] points)
        {
            if (pen != null)
            {
                var halfPenWidth = pen.Thickness / 2;
                var g = new GuidelineSet();
                foreach (var pt in points)
                {
                    g.GuidelinesX.Add(pt.X + halfPenWidth);
                    g.GuidelinesY.Add(pt.Y + halfPenWidth);
                    dc.PushGuidelineSet(g);
                }
                dc.DrawGeometry(brush, pen, pathGeometry);
                dc.Pop();
            }
            else
            {
                dc.DrawGeometry(brush, null, pathGeometry);
            }
        }
    }
}
