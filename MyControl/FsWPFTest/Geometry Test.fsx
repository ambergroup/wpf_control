﻿#r "System.Xaml"

#load "InstallWPFEventLoop.fsx"

open System
open System.Xml
open System.Drawing
open System.Windows
open System.Windows.Input
open System.Windows.Media
open System.Windows.Media.Imaging
open System.Windows.Shapes
open System.Windows.Controls
//open System.IO

let image_path = __SOURCE_DIRECTORY__ +  @"\Chrysanthemum.jpg"
let window = new Window(Title = "Simple Test", Width = 800., Height = 600.)
window.Show()

//canvas
let canvas = new Canvas()
window.Content <- canvas
canvas.Background <- Brushes.AliceBlue
canvas.Children.Clear()

//path
let path = new Path()
canvas.Children.Add(path)
path.Stroke <- Brushes.Black
path.StrokeThickness <- 2.0

//line geometry 
let linegmy = new LineGeometry()
path.Data <- linegmy    
linegmy.StartPoint <- Point(150.0, 30.0)
linegmy.EndPoint <- Point(200.0, 50.0)

//ellipse geometry
let ellipsegmy = new EllipseGeometry()
ellipsegmy.Center <- Point(102.0, 102.0)
ellipsegmy.RadiusX <- 100.0 
ellipsegmy.RadiusY <- 100.0 
path.Data <- ellipsegmy
path.Fill <- Brushes.Gold

//image
let image = new Image()
canvas.Children.Add(image)
let imageUri = new Uri(image_path)
image.Source <- new BitmapImage(imageUri)
image.Width <- 200.0
image.Height <- 150.0
let img_ellipsegmy = new EllipseGeometry()
img_ellipsegmy.Center <- Point(100.0, 75.0)
img_ellipsegmy.RadiusX <- 100.0 
img_ellipsegmy.RadiusY <- 75.0 
image.Clip <- img_ellipsegmy

/// Create a PathGeometry to contain the figure.
let myPathGeometry = new PathGeometry();
let myPathFigure = new PathFigure()
path.Data <- myPathGeometry
myPathGeometry.Figures.Add(myPathFigure);
//PathFigure
let points = [(10,20);(100,130);(200,130);(200,150);(300,150);(400,150)] |> Seq.map (fun (a,b)-> Point(double a, double b))

//Seq.head points
myPathFigure.IsClosed <- true
myPathFigure.StartPoint <- (points |> Seq.head)
myPathFigure.Segments.Add(new LineSegment(Point(100.0,130.0), true))
myPathFigure.Segments.Add(new LineSegment(Point(200.0,130.0), true))
points |> Seq.skip(1) 
       |> Seq.map (fun x-> Point(x.X + 100.0, x.Y + 100.0))
       |> Seq.iter (fun x -> myPathFigure.Segments.Add(new LineSegment(x, true)))
//myPathFigure.StartPoint <- Point(10.0, 20.0)       
//myPathFigure.Segments.[1] <- new LineSegment(Point(250.0, 150.0), true)
//myPathGeometry.GetArea()
let combinedGeometry = new CombinedGeometry(GeometryCombineMode.Intersect, myPathGeometry, ellipsegmy)
combinedGeometry.GeometryCombineMode <- GeometryCombineMode.Union   
combinedGeometry.StrokeContains(new Pen(), Point(102.0, 4.0))
path.Fill <- Brushes.Aqua
path.Data <- combinedGeometry   
path.MouseDown.Add(fun x -> printfn "%A" x)


let line = new Line() 
canvas.Children.Add(line)
line.X1 <- 10.0
line.Y1 <- 10.0
line.X2 <- 150.0
line.Y2 <- 150.0
line.StrokeThickness <- 5.0
line.Stroke <- Brushes.Green
line.MouseDown.Add(fun e -> printfn "%A" e.ClickCount);
