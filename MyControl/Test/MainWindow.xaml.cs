﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenPopup(object sender, RoutedEventArgs e)
        {
            Popup.IsOpen = true;
        }

        private void OpenPopup2(object sender, RoutedEventArgs e)
        {
            Popup.IsOpen = true;
            //lb.SelectedIndex = -1;
            Mouse.Capture(null);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //lb.Focusable = false;
            var item = lb.SelectedItem as ListBoxItem;
            //item.Focusable = false;
            //Popup.IsOpen = false;
            Mouse.Capture(item);
            Mouse.Capture(null);
        }
    }
}
