﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTileCanvas
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private TileVM Context;
        public MainWindow()
        {
            this.RenderTransform = matrixTransform;
            Context = new TileVM();
            this.DataContext = Context;
            InitializeComponent();
        }

        private MatrixTransform matrixTransform = new MatrixTransform();

        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            ZoomMethod3(sender as UIElement, e);
        }

        private void ZoomMethod3(UIElement sender, MouseWheelEventArgs e)
        {
            var element = sender;
            var position = e.GetPosition(element);
            var matrix = matrixTransform.Matrix;
            var scale = e.Delta >= 0 ? 1.1 : (1.0 / 1.1); 

            matrix.ScaleAtPrepend(scale, scale, position.X, position.Y);
            matrixTransform.Matrix = matrix;
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var p = Mouse.GetPosition((IInputElement) sender);
            Debug.WriteLine(p);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Context.TileWidth *= 2;
            Context.TileHeight *= 2;
            Context.RowCount /= 2;
            Context.ColCount /= 2;
            //Context.ColCount++;
            Context.OnPropertyChanged("RowCount");
            Context.OnPropertyChanged("ColCount");
        }
    }

    public class TileVM:INotifyPropertyChanged
    {
        public Dictionary<Rect, int> TilesDic { get; set; }

        public TileVM()
        {
            TilesDic = new Dictionary<Rect, int>();
            TilesDic.Add(new Rect(0,0, 20, 20), 1);
            TilesDic.Add(new Rect(20,0, 20, 20), 2);
            TilesDic.Add(new Rect(40,0, 20, 20), 3);
            TilesDic.Add(new Rect(60,0, 20, 20), 4);
            TilesDic.Add(new Rect(80,0, 20, 20), 5);
            TilesDic.Add(new Rect(100,0, 20, 20), 6);
            TilesDic.Add(new Rect(120,0, 20, 20), 7);
            RowCount = 5;
            ColCount = 5;
            TileHeight = 20;
            TileWidth = 20;
            TileCanvasWidth = 70;
            TileCanvasHeight = ColCount*TileHeight;
            TileLineThickness = TileWidth*0.01;
        }

        public int _TileWidth;
        public int _TileHeight;

        public int _TileCanvasWidth;
        public int _TileCanvasHeight;

        public int RowCount { get; set; } 
        public int ColCount { get; set; }

        private double _TileLineThickness = 0;

        public int SelectedTileId
        {
            get { return _SelectedTileId; }
            set
            {
                _SelectedTileId = value;
                OnPropertyChanged("SelectedTileId");
            }
        }

        public int TileWidth
        {
            get { return _TileWidth; }
            set
            {
                _TileWidth = value; 
                OnPropertyChanged("TileWidth");
            }
        }

        public int TileHeight
        {
            get { return _TileHeight; }
            set
            {
                _TileHeight = value; 
                OnPropertyChanged("TileHeight");
            }
        }

        public double TileLineThickness
        {
            get { return _TileLineThickness; }
            set
            {
                _TileLineThickness = value; 
                OnPropertyChanged("TileLineThickness");
            }
        }

        public int TileCanvasWidth
        {
            get { return _TileCanvasWidth; }
            set
            {
                _TileCanvasWidth = value; 
                OnPropertyChanged("TileCanvasWidth");
            }
        }

        public int TileCanvasHeight
        {
            get { return _TileCanvasHeight; }
            set
            {
                _TileCanvasHeight = value; 
                OnPropertyChanged("TileCanvasHeight");
            }
        }

        public int _SelectedTileId;

        public event PropertyChangedEventHandler PropertyChanged;
        
        public virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null) {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
