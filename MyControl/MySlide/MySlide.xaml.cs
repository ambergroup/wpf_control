﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestControl
{
    /// <summary>
    /// Interaction logic for MySlide.xaml
    /// </summary>
    public partial class MySlide : UserControl
    {
        public MySlide()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty RValueProperty = DependencyProperty.Register(
            "RValue", typeof (string), typeof (MySlide), new PropertyMetadata("JJJ"));

        public string RValue
        {
            get { return (string) GetValue(RValueProperty); }
            //set { SetValue(RValueProperty, value); }
        }
    }
}
