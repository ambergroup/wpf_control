﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MySlide
{

    public enum ProcessStage
    {
        Stage1,
        Stage2,
        Stage3
    }

    class ProgressHelper
    {
        public static readonly DependencyProperty ProcessCompletionProperty = DependencyProperty.RegisterAttached(
            "ProcessCompletion", typeof (ProcessStage), typeof (ProgressHelper), new PropertyMetadata(default(ProcessStage)));

        private static void OnProcessCompletionChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            double progress = (double) e.NewValue;
            ProgressBar pb = s as ProgressBar;

        }

        public static void SetProcessCompletion(DependencyObject element, ProcessStage value)
        {
            element.SetValue(ProcessCompletionProperty, value);
        }

        public static ProcessStage GetProcessCompletion(DependencyObject element)
        {
            return (ProcessStage) element.GetValue(ProcessCompletionProperty);
        }
    }
}
