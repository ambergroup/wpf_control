﻿module TestWindow

#r "System.Xaml"

#load "InstallWPFEventLoop.fsx"


open System
open System.Xml
open System.Drawing
open System.Windows
open System.Windows.Input
open System.Windows.Media
open System.Windows.Shapes
open System.Windows.Controls

let window = new Window(Title = "Simple Test", Width = 800., Height = 600.)

let canvas = new Canvas()
let grid = new Grid()
let rect = new Rectangle()
rect.Width <- 100.0
rect.Height <- 100.0
rect.Fill <- Media.Brushes.Red
//grid.Children.Add(rect)
//grid.Children.Clear()

let line = new Line() 
canvas.Children.Add(line)
line.X1 <- 10.0
line.Y1 <- 10.0
line.X2 <- 150.0
line.Y2 <- 150.0
line.StrokeThickness <- 5.0
line.Stroke <- Brushes.Green
line.MouseDown.Add(fun e -> printfn "%A" e.ClickCount);

window.Content <- canvas
canvas.HorizontalAlignment <- HorizontalAlignment.Stretch
canvas.VerticalAlignment <- VerticalAlignment.Stretch
canvas.Background <- Brushes.AliceBlue

window.Show()

let TestHit control =
    new MouseButtonEventHandler(
        fun (s:obj) (e:MouseButtonEventArgs) -> 
            let pt = e.GetPosition(s :?> UIElement)
            let result  = VisualTreeHelper.HitTest(control, pt)
            if not <| obj.ReferenceEquals(result, null) then
                printfn "null"
            else
                printfn "%A" result)


//let RecordHit =
//    new MouseButtonEventHandler(
//        fun (s:obj) (e:MouseButtonEventArgs) -> 
//            let pt = e.GetPosition(s :?> UIElement)
//            let l = []
//            let result  = VisualTreeHelper.HitTest(window, null,
//                 ,pt)
//            if not <| obj.ReferenceEquals(result, null) then
//                printfn "null"
//            else
//                printfn "%A" result)
grid.MouseLeftButtonDown.AddHandler(TestHit grid)
grid.MouseLeftButtonDown.RemoveHandler(TestHit grid)

grid.IsHitTestVisible
rect.IsHitTestVisible
window.IsHitTestVisible

//let textBox = new TextBox(Text = "F# is fun")

