﻿#r "System.Xml.Linq"
open System
open System.Text
open System.Runtime.InteropServices
open System.IO
open System.Drawing
open System.Xml.Linq
open System.Xml.XPath
open System.Xml

let xn s = XName.Get(s)
let xname s = XName.op_Implicit(s)

let path = @"c:\Users\root\Documents\ThorImageLS 3.1\Application Settings"
let hardwarepath = path + @"\HardwareSettings.xml"
let doc = XDocument.Load(hardwarepath).Root
let cc =  doc.Element(xname "ColorChannels")

cc.Elements() |> Seq.map (fun x-> ((x.Attribute(xn "name")).Value, x.Name.LocalName)) 
              |> Seq.where (fun x-> (fst x).ToLower() <>  "none")
              |> Seq.iter (printfn "%A")


id 100

let filepath = path + @"\luts\Rainbow.txt"

Directory.GetFiles(path) |> Seq.map (fun x -> FileInfo(x).CreationTime)

let fs = new StreamReader(filepath)        
fs.BaseStream.Length
let content = fs.ReadToEnd()
let r = content.TrimEnd().Split([|'\r';'\n'|], StringSplitOptions.RemoveEmptyEntries)
            |> Seq.take 256
            |> Seq.map(fun s -> s.Split(',') |> Seq.map byte ) 
            |> Seq.collect (fun x -> x)
//            |> Seq.map(fun barry -> Color.FromArgb(0,int barry.[0],int barry.[1],int barry.[2]))

r |> Seq.toList |> Seq.take 9 |> Seq.iter (printf "%A ")
r |> Seq.last
r |> Seq.length


Color.FromArgb(1).ToString();
Color.AliceBlue.ToString();

System.Environment.GetEnvironmentVariable("USERPROFILE")
let x = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments)
Directory.GetDirectories(x)
Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms)
Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)
Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)

let cl = 
    using(new StreamReader(filepath))
        (fun fs -> 
            let content = fs.ReadToEnd()
            let r = content.TrimEnd().Split([|'\r';'\n'|], StringSplitOptions.RemoveEmptyEntries)
                        |> Seq.map(fun s -> s.Split(',') |> Seq.map byte) 
                        |> Seq.collect (fun x -> x)
            r)
//                        |> Seq.map(fun barry -> Color.FromArgb(0,barry.[0],barry.[1],barry.[2]))
module Seq = 
    let rec chunks n (s:#seq<_>) =
        seq {
                 if Seq.length s <= n then
                    yield s
                 else
                    yield Seq.take n s
                    yield! chunks n (Seq.skip n s)           
            }

let l = [1;2;3;4]
l |> Seq.fold (fun s x-> x :: s ) [] 
l |> Seq.scan (fun s x-> x + s) 0 |> Seq.iter (printf "%d ")
l |> Seq.chunks 2
[|1;2;3;4;5;6;7|] |> Seq.take 1 |> Seq.take 1   
|> Seq.chunks 3 |> Seq.map (fun x -> ((Seq.take 1 x), (Seq.take 1 x)))
let a = { let x = "gg"
          new IDisposable with
              member x.Dispose() = 
              printfn "Hello Dispose" + x}

//[].Add(70)
[(1,2);(3,4);(6,1);(7,5)] @ [(8,0)]
|> Seq.pairwise 
|> Seq.map (fun (b,e) -> 
       [for i in fst b..fst e-1 do 
            if i = fst b then
                yield b
            else 
                yield (i,0)]) 
|> Seq.collect id |> Seq.iter (printf "%A ")
                    
using(a)(fun x -> id x)                    