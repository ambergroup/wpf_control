﻿#I __SOURCE_DIRECTORY__
#I @"..\DPinUserControl\packages\FSharp.Charting.0.90.14\"
#I @"..\DPinUserControl\packages\FSharp.Collections.ParallelSeq.1.0.2\lib\net40\"
#load "Fsharp.Charting.fsx"
#r "FSharp.Collections.ParallelSeq.dll"

#time

open System.Drawing
open System.Windows.Forms
open System.Drawing.Imaging//ImageLockMode
open System.Runtime.InteropServices
open FSharp.Charting
open System
open FSharp.Collections.ParallelSeq

module Seq = 
//    let rec chunks n (s:#seq<_>) =
//        seq{  
//                 if Seq.length s <= n then
//                    yield s
//                 else
//                    yield Seq.take n s 
//                    yield! chunks n (Seq.skip n s)           
//           } 
    let chunk n s =
        seq {
            let r = ResizeArray<_>()
            for x in s do
                r.Add(x)
                if r.Count = n then
                    yield r.ToArray()
                    r.Clear()
            if r.Count <> 0 then
                yield r.ToArray()
        }

let image_path = __SOURCE_DIRECTORY__ +  @"\Chrysanthemum.jpg"
let bitmap = new Bitmap(image_path)

let imagerect = new Rectangle(0,0,bitmap.Width, bitmap.Height)
printfn "%A" image_path
bitmap.GetPixel(0,0)

let bytesgray (s:byte list) = 
    match s with
    |[b;g;r] -> (byte)((9798 * (int r) + 19235 * (int g) + 3735 * (int b)) / 32768)
    |_ -> 127uy

bitmap.Width * bitmap.Height/3

let bitmapdata = bitmap.LockBits(imagerect, ImageLockMode.ReadWrite, bitmap.PixelFormat)
bitmapdata.Stride
let buffer:byte[] = Array.zeroCreate (bitmap.Width * bitmap.Height * Bitmap.GetPixelFormatSize(bitmap.PixelFormat)/8)
Marshal.Copy(bitmapdata.Scan0, buffer, 0,  buffer.Length)

//let newbuff:byte[] = Array.zeroCreate (bitmap.Width * bitmap.Height * Bitmap.GetPixelFormatSize(bitmap.PixelFormat)/8)
//
let length = (bitmap.Width * bitmap.Height * Bitmap.GetPixelFormatSize(bitmap.PixelFormat)/8)
//for i in 0..3..length-1 do
//    let g = (byte)((9798 * (int buffer.[i+2]) + 19235 * (int buffer.[i+1]) + 3735 * (int buffer.[i])) / 32768)
//    newbuff.[i] <- g
//    newbuff.[i + 1] <- g
//    newbuff.[i + 2] <- g
//                                    
//let newbuff = buffer |> Seq.chunk 3 
//                     |> Seq.map (fun x -> bytesgray (x |> Seq.toList)) 
//                     |> Seq.map (fun x -> List.init 3 (fun _ -> x))
//                     |> Seq.collect (fun x -> x)
//                     |> Array.ofSeq

//let rArray = [|for i in 0 .. 255 -> 0uy|]
//buffer 
//       |> Seq.chunk 3 
//       |> Seq.map (fun x -> bytesgray (x |> Seq.toList)) 
//       |> Seq.map (fun x -> List.init 3 (fun _ -> x))
//       |> Seq.collect (fun x -> x)
let rArray = 
       buffer  
       |> Seq.groupBy id |> Seq.map (fun x -> (fst x), double (Seq.length (snd x))/ double length)
       |> Seq.append [255uy, 0.0]
       |> Seq.sortBy id
       |> Seq.pairwise
       |> Seq.map (fun (b,e) -> 
                       [for i in fst b..fst e-1uy do 
                           if i = fst b then
                               yield b
                           else 
                               yield (i, 0.0)]) 
       |> Seq.collect id                                
       |> Seq.scan (fun op (k, p) -> (p + op)) (0.0)
       |> Seq.map (fun p -> (byte)(255.0 * p + 0.5))
       |> Seq.toArray

//#time
//(2I**3000).ToString().ToCharArray()
//       |> Seq.scan (fun (_, op) (k,p)-> (k, p + op)) (0uy, 0.0)
//       |> Seq.map (fun (k, p) -> (int k, (byte)(255.0 * p + 0.5)))
//       |> Seq.iter (fun (k, p) -> rArray.[k] <- p)

let newbuff = buffer |> Seq.map(fun x -> rArray.[int x]) |> Array.ofSeq

//let newbuff = buffer |> Seq.map(fun x -> 255uy - x) |> Array.ofSeq

Marshal.Copy(newbuff, 0, bitmapdata.Scan0, buffer.Length)
bitmap.UnlockBits(bitmapdata)
image.Invalidate()

let form = new Form()
let image = new PictureBox(Dock = DockStyle.Fill)
form.Controls.Add(image)
image.Image <- bitmap 
image.SizeMode <- PictureBoxSizeMode.StretchImage
form.Show()

let Pixels =seq {for h in 0..bitmap.Height-1 do
                    for w in 0..bitmap.Width-1 do
                         yield bitmap.GetPixel(w, h)}

Pixels |> Seq.iteri(fun i c -> bitmap.SetPixel(i%bitmap.Width,i/bitmap.Width, Color.FromArgb((255 - (int c.R)), (255 - (int c.G)), (255 - (int c.B)))))

let width = bitmap.Width
let GrayValues = Pixels |> Seq.map(fun x -> (9798 * (int x.R) + 19235 * (int x.G) + 3735 *(int x.B)) /32768) 
                        |> Seq.toArray
                        |> Seq.mapi(fun i x -> (i%width, i/width, Color.FromArgb(int x,int x, int x)))
                        |> Seq.iter(fun (x,y,z) -> bitmap.SetPixel(x,y,z))

GrayValues |> Seq.iteri(fun i x -> bitmap.SetPixel(i%bitmap.Width, i/bitmap.Width, Color.FromArgb(int x,int x,int x)))

GrayValues |> Seq.mapi(fun i x -> (i%width, i/width, Color.FromArgb(int x,int x, int x)))

GrayValues |> PSeq.mapi(fun i x -> (i%width, i/width, Color.FromArgb(int x,int x, int x)))
           |> Seq.iter(fun (x,y,z) -> bitmap.SetPixel(x,y,z))

let improve = (955.0-791.0)/955.0

let revert (bitmap:Bitmap) x y = 
    let c = bitmap.GetPixel(x, y)
    let rc = Color.FromArgb((255 - (int c.R)), (255 - (int c.G)), (255 - (int c.B)))
    bitmap.SetPixel(x, y, rc)

let revert (c:Color) = 
    Color.FromArgb((255 - (int c.R)), (255 - (int c.G)), (255 - (int c.B)))

Pixels |> Seq.toArray 
       |> PSeq.mapi(fun i x -> (i%width, i/width, (revert x)))
       |> Seq.iter(fun (x,y,z) -> bitmap.SetPixel(x,y,z))

for w in 0..bitmap.Width-1 do
     for h in 0..bitmap.Height-1 do
        revert bitmap w h


//let greyPixel (bitmap:Bitmap) x y = 
//    let c = bitmap.Getp 
//let s = [-1;0;1]
//let r = s |> Seq.collect(fun x -> s |> Seq.map( fun y -> x,y) ) 
//          |> Seq.filter (fun x -> x <> (0,0))

//let fd (bitmap:Bitmap) (range:seq<int*int>) x y =
//    let width = bitmap.Width
//    let height = bitmap.Height
//    
//    range |> Seq.map (fun (x1,y1)-> (x1+x, y1+y)) 
//          |> Seq.filter (fun (x1, y1) -> 0 <= x1 && x1 < width && 0 <= y1 && y1 < height)
//          |> Seq.map (fun (x1, y1) -> bitmap.GetPixel(x1, y1)



let rArray = [|for i in 0 .. 255 -> 0|]
rArray |> Seq.

let size = (double)(bitmap.Width * bitmap.Height)
let sResult = 
    GrayValues |> Seq.iter (fun x-> rArray.[x] <- rArray.[x] + 1) 
let cdf = rArray |> Seq.map (double >> (fun x-> x /size)) |> Seq 
|> Seq.pairwise
cdf 

Chart.Column [for x in 0..255 -> (double)rArray.[x]/size]

