﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CustomPanel
{
    class MyPanel:Panel
    {
        protected override Size MeasureOverride(Size availableSize)
        {
            var count = InternalChildren.Count;
            foreach (UIElement item in InternalChildren)
            {
                item.Measure(new Size(availableSize.Width / count, availableSize.Height / count));
            }
            return base.MeasureOverride(availableSize);
        }

        private bool IsDoubleEqual(double a, double b)
        {
            return Math.Abs(a - b) > double.Epsilon ? false : true;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            double top = 0;
            double left = 0;
            var count = InternalChildren.Count;
            var eWidth = finalSize.Width / count;
            var eHeight = finalSize.Height / count;
            foreach (UIElement item in InternalChildren)
            {
                if (!(IsDoubleEqual(top, 0) && IsDoubleEqual(left, 0)))
                {
                    top -= 10;
                    left -= 10;
                }
                item.Arrange(new Rect(new Point(left, top), new Size(eWidth, eHeight)));
                top += eHeight;
                left += eWidth;
            }
            return base.ArrangeOverride(finalSize);
        }

        protected override int VisualChildrenCount
        {
            get { return base.VisualChildrenCount; }
        }

        protected override Visual GetVisualChild(int index)
        {
            return Children[Children.Count - 1 - index];
        }
    }
}
