﻿using System;
using DPinUserControl.Model;

namespace DPinUserControl.Design
{
    public class DesignDataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to create design time data

            var item = new DataItem("Heelloo");
            callback(item, null);
        }
    }
}