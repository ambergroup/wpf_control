﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CustomClockControl
{
    public class Arc:Shape
    {
        public static readonly DependencyProperty StartAngleProperty = DependencyProperty.Register(
            "StartAngle", typeof (double), typeof (Arc), new PropertyMetadata(default(double)));

        public double StartAngle
        {
            get { return (double) GetValue(StartAngleProperty); }
            set { SetValue(StartAngleProperty, value); }
        }

        public static readonly DependencyProperty EndAngleProperty = DependencyProperty.Register(
            "EndAngle", typeof (double), typeof (Arc), new FrameworkPropertyMetadata(default(double), FrameworkPropertyMetadataOptions.AffectsRender));

        private static void OnEndAngle(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            Debug.WriteLine(e.NewValue);
        }

        public double EndAngle
        {
            get { return (double) GetValue(EndAngleProperty); }
            set { SetValue(EndAngleProperty, value); }
        }

        private Point AngleToPoint(double pAngle)
        {
            double radAngle = pAngle * (Math.PI / 180);
			double xRadius = (RenderSize.Width - StrokeThickness)/2;
			double yRadius = (RenderSize.Height - StrokeThickness)/2;

			double x = xRadius + xRadius*Math.Cos(radAngle);
			double y = yRadius - yRadius*Math.Sin(radAngle);

			return new Point(x, y);
        }

        protected override Geometry DefiningGeometry
        {
            get {
                return GetGeometry();
            }
        }

        private Geometry GetGeometry()
        {
            Point startPoint = AngleToPoint(StartAngle);
            Point endPoint = AngleToPoint(EndAngle);

			Size arcSize = new Size(Math.Max(0, (RenderSize.Width - StrokeThickness)/2),
			                        Math.Max(0, (RenderSize.Height - StrokeThickness)/2));
			bool isLargeArc = Math.Abs(EndAngle - StartAngle) > 180;

            StreamGeometry sg = new StreamGeometry();
            using (var context = sg.Open())
            {
                context.BeginFigure(startPoint, false, false);
                context.ArcTo(endPoint, arcSize, 0, isLargeArc, SweepDirection.Counterclockwise, true, false );
            }
            sg.Transform = new TranslateTransform(StrokeThickness/2, StrokeThickness/2);
            return sg;

        }
    }
}
