﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Xaml.Schema;
using DPinUserControl.Design;
using GalaSoft.MvvmLight;
using DPinUserControl.Model;
using GalaSoft.MvvmLight.Command;

namespace DPinUserControl.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";

        private string _welcomeTitle;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }
            set
            {
                Set(ref _welcomeTitle, value);
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            CommandEnable = false;
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    WelcomeTitle = item.Title;
                });
            DummyCommand = new MyRelayCommand(()=>{}, () =>{ return CommandEnable;});
            TestCollection = new ObservableCollection<int>();
            SlideValue = 5;
            SlideMinValue = 0;
            SlideMaxValue = 20;
        }

        private bool isOnce = true;

        public ICommand ChangeCommandState
        {
            get
            {
                return new RelayCommand(() =>
                {
                    SlideValue = 14;
                    //CommandEnable = !CommandEnable;
                    //CommandManager.InvalidateRequerySuggested();
                    //WelcomeTitle += "X";


                    //if (isOnce)
                    //{
                    //    ACollection.Add(new Random(1).Next());
                    //    ACollection.Add(new Random(12).Next());
                    //    TestCollection = ACollection;
                    //    isOnce = false;
                    //}
                    //else
                    //{
                    //    TestCollection = new ObservableCollection<int>(ACollection.Concat(ACollection).ToList());
                    //}
                    //RaisePropertyChanged("TestCollection");

                    //DummyCommand.RaiseCanExecuteChanged();
                    //RaisePropertyChanged(()=>DummyCommand);
                    //RaisePropertyChanged(()=>CommandEnable);
                });
            }
        }

        public ObservableCollection<int> ACollection = new ObservableCollection<int>();

        public ObservableCollection<int> TestCollection { get; set; }

        public ICommand DummyCommand { get; set; }

        public bool CommandEnable { get; set; }

        private int _slideValue;

        public int SlideValue
        {
            get
            {
                return _slideValue;
            }
            set
            {
                Debug.WriteLine("mvvm Slide Value changed");
                //should check the min and max value;
                _slideValue = value;
                RaisePropertyChanged(() => SlideValue);
            }
        }

        public int SlideMinValue { get; set; }
        public int SlideMaxValue { get; set; }

        public GradientBrush ColorBrush
        {
            get
            {
                LinearGradientBrush gb = new LinearGradientBrush();
                gb.StartPoint = new Point(0, 0);
                gb.EndPoint = new Point(1, 1);
                var stop1 = new GradientStop()
                {
                    Color = Colors.Blue,
                    Offset = 0.0
                };
                var stop2 = new GradientStop()
                {
                    Color = Colors.Red,
                    Offset = 0.5
                };
                gb.GradientStops = new GradientStopCollection(new List<GradientStop> {stop1, stop2});
                return gb;
            }
        }
        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}