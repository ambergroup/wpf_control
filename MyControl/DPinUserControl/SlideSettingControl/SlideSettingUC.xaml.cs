﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SlideSettingControl
{
    public partial class SlideSettingUC : UserControl
    {
        public SlideSettingUC()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value", typeof (int), typeof (SlideSettingUC), new PropertyMetadata(default(int)));

        public int Value
        {
            get { return (int) GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty MaxValueProperty = DependencyProperty.Register(
            "MaxValue", typeof (int), typeof (SlideSettingUC), new PropertyMetadata(18));

        public int MaxValue
        {
            get { return (int) GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public static readonly DependencyProperty MinValueProperty = DependencyProperty.Register(
            "MinValue", typeof (int), typeof (SlideSettingUC), new PropertyMetadata(default(int)));

        public int MinValue
        {
            get { return (int) GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && e.Key == Key.Enter)
            {
                tTextBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                Keyboard.ClearFocus();
            }
        }
    }

    public class SliderIgnoreDelta : Slider
    {
        public int FinalValue
        {
            get { return (int)GetValue(FinalValueProperty); }
            set { SetValue(FinalValueProperty, value); }
        }

        public static readonly DependencyProperty FinalValueProperty =
            DependencyProperty.Register(
                "FinalValue", typeof(int), typeof(SliderIgnoreDelta),
                new FrameworkPropertyMetadata(0,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnFinalValueChanged));

        private static void OnFinalValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            int result;
            if (int.TryParse(e.NewValue.ToString(), out result))
            {
                if (((SliderIgnoreDelta) sender).Value != result)
                {
                    ((SliderIgnoreDelta) sender).Value = result;
                }
            }
        }

        public bool IsDragging { get; protected set; }
        protected override void OnThumbDragCompleted(System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            IsDragging = false;
            base.OnThumbDragCompleted(e);
            OnValueChanged(Value, Value);
        }

        protected override void OnThumbDragStarted(System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            IsDragging = true;
            base.OnThumbDragStarted(e);
        }

        protected override void OnValueChanged(double oldValue, double newValue)
        {
            if (!IsDragging)
            {
                base.OnValueChanged(oldValue, newValue);
                if (FinalValue != (int)Math.Round(Value, 0))
                {
                    FinalValue = (int)Math.Round(Value, 0);
                }
            }
        }

    }


    public class TextConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Debug.WriteLine(value);
            try
            {
                if((double)value == 0)
                {
                    return "Null";
                }
                else
                {
                    return Math.Round((double)value, 0);
                }
            }
            catch (InvalidCastException)
            {
                return "Null";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int result = 0;
            if (int.TryParse(value.ToString(), out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }
    }
}
