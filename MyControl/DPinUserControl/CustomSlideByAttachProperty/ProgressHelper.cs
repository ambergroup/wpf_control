﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace CustomSlideByAttachProperty
{
    public enum ProgressStage
    {
        Stage1,
        Stage2,
        Stage3
    }

    class ProgressHelper
    {
        public static readonly DependencyProperty ProgressValueProperty = DependencyProperty.RegisterAttached(
            "ProgressValue", typeof (double), typeof (ProgressHelper), new PropertyMetadata(default(double), new PropertyChangedCallback(OnProgressValueChange)));


        public static void SetProgressValue(DependencyObject element, double value)
        {
            //the value is not used by anyone.
            //element.SetValue(ProgressValueProperty, value);
        }

        public static double GetProgressValue(DependencyObject element)
        {
            return (double) element.GetValue(ProgressValueProperty);
        }

        private static readonly DependencyPropertyKey ProgressStagePropertyKey = DependencyProperty.RegisterAttachedReadOnly(
            "ProgressStage", typeof (ProgressStage), typeof (ProgressHelper), new PropertyMetadata(default(ProgressStage)));

        //Set or Get must have one
        //public static void SetProgressStage(DependencyObject element, ProgressStage value)
        //{
        //    element.SetValue(ProgressStagePropertyKey, value);
        //}

        public static ProgressStage GetProgressStage(DependencyObject element)
        {
            return (ProgressStage)element.GetValue(ProgressStagePropertyKey.DependencyProperty);
        }

        public static readonly DependencyProperty ProgressStageProperty = ProgressStagePropertyKey.DependencyProperty;

        private static void OnProgressValueChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var v = (double)e.NewValue;
            var bar = d as ProgressBar;
            if (0 <= v && v <= 33)
            {
                //both ok
                bar.SetValue(ProgressStagePropertyKey, ProgressStage.Stage1);
               //SetProgressStage(d, ProgressStage.Stage1); 
            }
            else if (33 < v && v <= 66)
            {
                bar.SetValue(ProgressStagePropertyKey, ProgressStage.Stage2);
               //SetProgressStage(d, ProgressStage.Stage2); 
            }
            else
            {
                bar.SetValue(ProgressStagePropertyKey, ProgressStage.Stage3);
               //SetProgressStage(d, ProgressStage.Stage3); 
            }
        }
    }
}
