﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomControl
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CustomControl"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CustomControl;assembly=CustomControl"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:ZoomAndPanControl/>
    ///
    /// </summary>
    public class ZoomAndPanControl : ContentControl, IScrollInfo 
    {
        static ZoomAndPanControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ZoomAndPanControl), new FrameworkPropertyMetadata(typeof(ZoomAndPanControl)));
        }

		private bool _canHScroll = false;
		private bool _canVScroll = false;

        bool IScrollInfo.CanHorizontallyScroll
        {
            get { return _canHScroll; }
            set { _canHScroll = value; }
        }

        bool IScrollInfo.CanVerticallyScroll
        {
            get { return _canVScroll; }
            set { _canVScroll = value; }
        }

        double IScrollInfo.ExtentHeight
        {
            get { throw new NotImplementedException(); }
        }

        double IScrollInfo.ExtentWidth
        {
            get { throw new NotImplementedException(); }
        }

        double IScrollInfo.HorizontalOffset
        {
            get { throw new NotImplementedException(); }
        }

        void IScrollInfo.LineDown()
        {
            throw new NotImplementedException();
        }

        void IScrollInfo.LineLeft()
        {
            throw new NotImplementedException();
        }

        void IScrollInfo.LineRight()
        {
            throw new NotImplementedException();
        }

        void IScrollInfo.LineUp()
        {
            throw new NotImplementedException();
        }

        Rect IScrollInfo.MakeVisible(Visual visual, Rect rectangle)
        {
            //throw new NotImplementedException();
            return new Rect();
        }

        void IScrollInfo.MouseWheelDown()
        {
            //throw new NotImplementedException();
        }

        void IScrollInfo.MouseWheelLeft()
        {
            throw new NotImplementedException();
        }

        void IScrollInfo.MouseWheelRight()
        {
            //throw new NotImplementedException();
        }

        void IScrollInfo.MouseWheelUp()
        {
            //throw new NotImplementedException();
        }

        void IScrollInfo.PageDown()
        {
            throw new NotImplementedException();
        }

        void IScrollInfo.PageLeft()
        {
            throw new NotImplementedException();
        }

        void IScrollInfo.PageRight()
        {
            throw new NotImplementedException();
        }

        void IScrollInfo.PageUp()
        {
            throw new NotImplementedException();
        }

        ScrollViewer IScrollInfo.ScrollOwner { get; set; }

        void IScrollInfo.SetHorizontalOffset(double offset)
        {
            throw new NotImplementedException();
        }

        void IScrollInfo.SetVerticalOffset(double offset)
        {
            throw new NotImplementedException();
        }

        double IScrollInfo.VerticalOffset
        {
            get { throw new NotImplementedException(); }
        }

        double IScrollInfo.ViewportHeight
        {
            get { throw new NotImplementedException(); }
        }

        double IScrollInfo.ViewportWidth
        {
            get { throw new NotImplementedException(); }
        }
    }
}
