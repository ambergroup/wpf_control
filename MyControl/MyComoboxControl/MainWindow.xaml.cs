﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyComoboxControl
{
    public class Item
    {
        public Brush ColorItem
        {
            get; set; }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window,INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            TestBrush = TestColors[1];
        }

        public List<byte[]> TestColors
        {
            get
            {
                return new List<byte[]>()
                {          
                    new byte[] { 0, 0, 0, 213, 21, 45,23,45,67,  123,33, 64, 255,255,255 },
                    new byte[] { 0, 0, 0, 255,0,0},
                    new byte[] { 0, 0, 0, 0,255,0},
                    new byte[] { 0, 0, 0, 0,0,255},
                    new byte[] { 0, 0, 0, 213, 21, 45,23,45,67,  123,33, 64, 255,255,255 },
                    new byte[] { 0, 0, 0, 255,0,0},
                    new byte[] { 0, 0, 0, 0,255,0},
                    new byte[] { 0, 0, 0, 0,0,255}
                };
            }
        }

        private byte[] _testBrush;

        public byte[] TestBrush
        {
            get { return _testBrush; }
            set
            {
                _testBrush = value;
                OnPropertyChanged("TestBrush");
            }
        }

        public static Brush test(byte[] byteColorList)
        {
            //byte[] byteColorList = new byte[] { 0, 0, 0, 213, 21, 45,23,45,67,  123,33, 64, 255,255,255 };
            LinearGradientBrush gb = new LinearGradientBrush()
            {
                StartPoint = new Point(0, 0),
                EndPoint = new Point(1, 0)
            };
            List<GradientStop> ColorList = new List<GradientStop>(); 
            double offset = 0, step = 1.0 / (byteColorList.Length / 3 -1);
            for (int i = 0; i < byteColorList.Length; i += 3)
            {
                if (i + 2 >= byteColorList.Length)
                    break;

                ColorList.Add(new GradientStop(){
                    Color= Color.FromRgb(
                        byteColorList[i + 2],
                        byteColorList[i + 1],
                        byteColorList[i]),
                    Offset = offset});
                offset += step;
            }
            gb.GradientStops = new GradientStopCollection(ColorList);
            return gb;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string pPropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(pPropertyName));
            }
        }
    }

    public class MyCombox : ComboBox
    {
        protected override void OnDropDownOpened(EventArgs e)
        {
            base.OnDropDownOpened(e);
            Debug.WriteLine("DropDown");
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            Debug.WriteLine("Click");
        }
    }


}
